from django.http import HttpResponse


class TokenMiddleware:
    TOKEN_KEY = "X-Token"
    TOKEN_VAL = "Evreka20210508"

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if self.TOKEN_KEY not in request.headers:
            return HttpResponse(status=401)

        token = request.headers[self.TOKEN_KEY]
        if token != self.TOKEN_VAL:
            return HttpResponse(status=401)

        return self.get_response(request)
