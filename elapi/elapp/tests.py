from django.test import TestCase
from elapp.models import Vehicle, NavigationRecord


class ModelsTest(TestCase):
    def test(self):
        new_vehicle = Vehicle.objects.create(plate="06EVR01")
        self.assertEqual(new_vehicle.plate, "06EVR01")
        new_vehicle.save()

        new_navigation_data = NavigationRecord(Vehicle=new_vehicle, latitude=32.32, longitude=32.32)
        self.assertEqual(new_navigation_data.Vehicle, new_vehicle)
        self.assertIsNotNone(new_navigation_data.datetime)
        new_navigation_data.save()
