from django.db import models
from datetime import datetime


class Vehicle(models.Model):
    class Meta:
        db_table = 'vehicle'

    plate = models.CharField(max_length=10)


class NavigationRecord(models.Model):
    class Meta:
        db_table = 'navigation_record'

    Vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    datetime = models.DateTimeField(default=datetime.now, blank=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
