from datetime import datetime, timedelta
from django.http import HttpResponse
from elapp.models import Vehicle, NavigationRecord

import json


def index(request):
    return HttpResponse('Evreka Localization API', content_type='text/html')


def getLatest(request):
    boundary = datetime.now() - timedelta(hours=48)
    nav_dataset = NavigationRecord.objects.filter(datetime__gte=boundary)
    response = []
    for nav_data in nav_dataset:
        response.append(
            {
                'latitude': round(nav_data.latitude, 2),
                'longitude': round(nav_data.longitude, 2),
                'vehicle_plate': nav_data.Vehicle.plate,
                'datetime': nav_data.datetime.strftime("%d.%m.%Y %H:%M:%S"),
            }
        )
    return HttpResponse(json.dumps([{'last_points': response}]), content_type='text/json')
